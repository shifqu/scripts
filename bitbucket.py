from collections import namedtuple
import os

from oauthlib.oauth2 import BackendApplicationClient
from requests_oauthlib import OAuth2Session
from dotenv import load_dotenv

load_dotenv(verbose=True)

KEY = os.environ["KEY"]
SECRET = os.environ["SECRET"]
TOKEN_URL = 'https://bitbucket.org/site/oauth2/access_token'


class Bitbucket:
    def __init__(self, r):
        self.__auth__()
        self._generate_urls(slug=r)

    def __auth__(self):
        self.client = BackendApplicationClient(client_id=KEY)
        self.oauth = OAuth2Session(client=self.client)
        self.token = self.oauth.fetch_token(
            token_url=TOKEN_URL, client_id=KEY, client_secret=SECRET
        )

    def _generate_urls(self, slug=False):
        Api = namedtuple('Api',
                         ('endpoint', 'version', 'username',
                          'repositories'))
        endpoint = 'https://api.bitbucket.org'
        version = '2.0'
        username = 'shifqu'
        repositories = f'{endpoint}/{version}/repositories/{username}'
        api = Api(endpoint, version, username, repositories)

        Repo = \
            namedtuple('Repo',
                       ('slug', 'me', 'refs', 'branches',
                        'tags', 'forks', 'src', 'issues',
                        'pullrequests'))
        me = f'{repositories}/{slug}'
        refs = f'{me}/refs'
        branches = f'{me}/refs/branches'
        tags = f'{me}/refs/tags'
        forks = f'{me}/forks'
        src = f'{me}/src'
        issues = f'{me}/issues'
        pullrequests = f'{me}/pullrequests'
        repo = Repo(slug, me, refs, branches, tags, forks, src, issues, pullrequests)
        Urls = namedtuple('Urls', ('repo', 'api'))
        self.urls = Urls(repo, api)

    def _generate_namedtuple(self, resource, **kwargs):
        Obj = namedtuple(resource, kwargs.keys())
        finaldict = {}
        # This loop makes sure we make objects recursively
        for k, v in kwargs.items():
            if isinstance(v, dict):
                self_ = v.pop('self', {})
                v = self._generate_namedtuple(k, **{**v, **self_})
            finaldict[k] = v

        return Obj(**finaldict)

    def request(self, resource, urltype='repo', type_='get', **kwargs):
        """

        :param resource: See fields of namedtuple `Repo` in `_generate_urls`
        :param urltype: Either `repo` or `api`
        :param type_: 'get', 'post', 'put
        :return:
        """
        try:
            u = getattr(self.urls, urltype)
            url = getattr(u, resource)
            makerequest = getattr(self.oauth, type_)
        except AttributeError as e:
            raise AttributeError(f'Unknown url/resource: {e.args}')

        if type_ == 'post':
            r = makerequest(url, params=kwargs)
        else:
            r = makerequest(url)
        j = r.json()
        v = j['values']
        s = j['size']  # Avoid len call, it is in the response anyway
        return (self._generate_namedtuple(resource, **d)
                for d in v), s

    def _create(self, resource, **kwargs):
        self.request(resource, type_='post', **kwargs)


def main(reponame):
    bitbucket = Bitbucket(reponame)
    repofields = bitbucket.urls.repo._fields
    [print(f'{i}: {f}') for i, f in enumerate(repofields)]
    resource_index = input('Select resource to fetch by INDEX: ')
    selected_resource = repofields[int(resource_index)]
    resources, amount = bitbucket.request(selected_resource)
    filler = "*" * 10

    resource_list = []
    for i, resource in enumerate(resources):
        print(f'{filler} INDEX {i} {filler}')
        [print(f'{x}: {getattr(resource, x)}') for x in sorted(resource._fields)]  # noqa NamedTuple
        print()
        resource_list.append(resource)

    selected = input('Enter INDEX of resource to select: ')
    try:
        currentresource = resource_list[int(selected)]
        print(currentresource._fields)
    except (IndexError, TypeError):
        raise Exception(f'{selected} was an invalid INDEX')

    while True:
        item = currentresource
        try:
            s = input('>>> ')
        except KeyboardInterrupt:
            exit()
        if s == 'exit()':
            exit()
        if s.startswith('create_'):
            kw = {}
            for f in item._fields:
                kw[f] = input(f'Value for {f}: ')
            bitbucket._create(selected_resource, **kw)
        for x in s.split('.')[1:]:
            item = getattr(item, x, f'{item} has no attr {x}')
        if isinstance(item, tuple):
            print(filler, 'fields')
            print(item._fields)
        else:
            print(filler, 'item')
            print(item)


if __name__ == '__main__':
    main('flask-media-center')