import os
import struct
import xmlrpc.client
import urllib.request
import gzip
from optparse import OptionParser
from hashlib import md5

# Public static variables
ENDPOINT = 'http://api.opensubtitles.org/xml-rpc'
ERROR_STRING = '{} not found, please refer to :method:{} for possible options'
EXT_SRT = 'srt'
VALID_EXTENSIONS = ['avi', 'mkv', 'mp4', 'm4v', 'mov', 'mpg', 'wmv', 'rm']

# Private static variables
try:
    USERNAME = os.environ['OpenUser']
    m = md5()
    m.update(os.environ['OpenPW'].encode())
    PASSWORD = m.digest()
    LANGUAGE = os.environ['OpenLang']
    USERAGENT = os.environ['OpenUA']
except KeyError as e:
    USERNAME = ''
    PASSWORD = ''
    LANGUAGE = 'en'
    USERAGENT = 'TemporaryUserAgent'
    print('Some environment variables are not set. ({})'.format(e.args), '\nProceeding with anonymous user')


class OpenClient(object):

    def __init__(self):
        self.t, self.token = self.login()

    @staticmethod
    def login():
        """
        Credentials are fetched from the environment-variables OpenUser, OpenPW, OpenLang, OpenUA.
        Passwords get md5-hashed since we are not using https
        Calls opensubtitles :method:(LogIn())[http://trac.opensubtitles.org/projects/opensubtitles/wiki/XMLRPC#LogIn]
        returns: connection to the endpoint and a token to be used in further calls
        """
        t = xmlrpc.client.ServerProxy(ENDPOINT)
        login = t.LogIn(USERNAME, PASSWORD, LANGUAGE, USERAGENT)
        try:
            token = login['token']
        except KeyError:
            print('Could not login')
            print(login)
            exit(27)
        return t, token

    def get_opts(self):
        """ Constructs kwargs to be used in the application """
        values = self.add_opts()
        options = {}
        options['rootdir'] = values.rootdir
        options['sublanguageid'] = values.sublanguageid
        options['force_override'] = values.force_override
        # Add valid extensions and handle included and excluded
        sep = values.sep
        extensions = VALID_EXTENSIONS
        if values.include_extensions:
            extensions += values.include_extensions.split(sep)
        if values.exclude_extensions:
            extensions = {x for x in extensions if x not in values.exclude_extensions.split(sep)}
        options['valid_extensions'] = extensions
        # Add excluded dirs if dirs are passed
        if values.excluded_dirs:
            options['excluded_dirs'] = values.excluded_dirs.split(sep)
        return options

    @staticmethod
    def add_opts():
        """ Add CLI options """
        parser = OptionParser()
        parser.add_option("-r", "--root", 
                          dest="rootdir",
                          default='~/Personal/Media',
                          help="Directory to traverse, if the path contains spaces, be sure to enclose it in quotes")
        parser.add_option("-e", "--include-extensions",
                          dest="include_extensions", 
                          default=[],
                          help="Include specific extensions")
        parser.add_option("-x", "--exclude-extensions",
                          dest="exclude_extensions", 
                          default=[],
                          help="Exclude specific extensions")
        parser.add_option("-X", "--exclude-directories",
                          dest="excluded_dirs", 
                          default=[],
                          help="The dirnames in this list will not be scanned for media")
        parser.add_option("-l", "--lang",
                          dest="sublanguageid", 
                          default='eng',
                          help="Sets the preffered subtitle language")
        parser.add_option("-s", "--sep",
                          dest="sep", 
                          default=',',
                          help="Sets the separator with which you separate your incl or excl dirs or extenions")
        parser.add_option("-f", "--force",
                          dest="force_override",
                          default=False,
                          help="Forces the override of subtitles, defaults to False")
        # parse_args returns (values, args). We only need the values
        return parser.parse_args()[0]

    def traverse_directory(self, root, function_name='process_', handle_type='files', excluded_dirs=(), **kwargs):
        """
        function_name: currently supports 'process_'
        handle_type: currently supports 'files'
        """
        for currentdir, dirs, files in os.walk(root):
            dirs[:] = [d for d in dirs if d not in excluded_dirs]
            kwargs.update(currentdir=currentdir)
            kwargs.update(dirs=dirs)
            kwargs.update(files=files)
            getattr(self, function_name)(handle_type, **kwargs)

    def process_(self, t, **kwargs):
        method_name = 'process_' + t
        try:
            getattr(self, method_name)(**kwargs)
        except AttributeError as e:
            print(ERROR_STRING.format(e.args, 'traverse_directory'))

    def process_files(self, valid_extensions=('avi', ), force_override=False, **kwargs):
        filehash_output_file_dict = {}
        try:
            for f in kwargs['files']:
                file_ext = self.get_file_extension(f)
                if file_ext in valid_extensions:
                    filepath = kwargs['currentdir'] + os.sep + f
                    output_path = filepath.replace(file_ext, EXT_SRT)
                    if not force_override and os.path.isfile(output_path):
                        # Don't download subs for already existing subs if force_override is False
                        print('Subtitle for {} already present, skipping'.format(f))
                        continue
                    filehash = self.calculate_hash_from(filepath=filepath)
                    filehash_output_file_dict[filehash] = output_path
        except KeyError as e:
            print(ERROR_STRING.format(e.args, 'traverse_directory'))
        
        if not filehash_output_file_dict:
            print('No files found to process at {}'.format(kwargs['currentdir']))
            return

        search = self.search(filehash_output_file_dict.keys())
        # FIXME handle exceptions
        self.process_search(search['data'], filehash_output_file_dict)

    @staticmethod
    def get_file_extension(name):
        ext = name.split('.')[-1]
        return ext
    
    @staticmethod
    def calculate_hash_from(filepath):
        try:
            longlongformat = '<q'  # little-endian long long
            bytesize = struct.calcsize(longlongformat)

            f = open(filepath, "rb")

            filesize = os.path.getsize(filepath)
            hash = filesize

            if filesize < 65536 * 2:
                ex_msg = "{}'s filesize is too small, you sure this is an actual media file?".format(filepath)
                raise Exception(ex_msg)

            for x in range(int(65536/bytesize)):
                buffer = f.read(bytesize)
                (l_value,) = struct.unpack(longlongformat, buffer)
                hash += l_value
                hash = hash & 0xFFFFFFFFFFFFFFFF  # to remain as 64bit number

            f.seek(max(0, filesize-65536), 0)
            for x in range(int(65536/bytesize)):
                buffer = f.read(bytesize)
                (l_value,) = struct.unpack(longlongformat, buffer)
                hash += l_value
                hash = hash & 0xFFFFFFFFFFFFFFFF

            f.close()
            returnedhash = "%016x" % hash
            return returnedhash

        except(IOError):
            return "IOError"

    def search(self, filehash_list, sublanguageid='eng', **kwargs):
        params = [{'sublanguageid': sublanguageid, 'moviehash': h} for h in filehash_list]
        return self.t.SearchSubtitles(self.token, params)

    def process_search(self, data, filehash_output_file_dict):
        for key in filehash_output_file_dict.keys():
            res = [x for x in data if x['MovieHash'] == key]
            if not res:
                print('Could not find subs for {}'.format(filehash_output_file_dict[key]))
                continue
            item = self.get_item_with_highest_score(res)
            url = item['SubDownloadLink']
            itemhash = item['MovieHash']
            output_file = filehash_output_file_dict[itemhash]
            self.download_and_write_to_file(url, output_file)
            print('Downloaded subs for {}'.format(filehash_output_file_dict[key]))

    @staticmethod
    def get_item_with_highest_score(d):
        return max(d, key=lambda x: x['Score'])

    @staticmethod
    def download_and_write_to_file(url, output_file):
        with urllib.request.urlopen(url) as response, open(output_file, 'wb') as out_file:
            with gzip.GzipFile(fileobj=response) as uncompressed:
                data = uncompressed.read()  # a `bytes` object
                out_file.write(data)

    def main(self, rootdir="/tmp/", **kwargs):
        self.t, self.token = self.login()
        rootdir = os.path.expanduser(rootdir)
        self.traverse_directory(rootdir, function_name='process_', handle_type='files', **kwargs)            
        print('DONE')


if __name__ == '__main__':
    oc = OpenClient()
    options = oc.get_opts()
    oc.main(**options)
    